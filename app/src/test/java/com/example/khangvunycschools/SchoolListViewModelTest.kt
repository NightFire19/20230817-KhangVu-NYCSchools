package com.example.khangvunycschools

import com.example.khangvunycschools.data.repo.RepositoryImpl
import com.example.khangvunycschools.models.SchoolEntity
import com.example.khangvunycschools.util.MainDispatcherRule
import com.example.khangvunycschools.vm.SchoolListViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class SchoolListViewModelTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    lateinit var repository: RepositoryImpl
    private val testDispatcher = StandardTestDispatcher()
    private val mockSchoolEntity: SchoolEntity = Mockito.mock()
    private lateinit var viewModel: SchoolListViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        viewModel = SchoolListViewModel(repository)
    }

    @Test
    fun `test getSchoolList() success`() = runTest {
        val mockSchools = listOf(mockSchoolEntity, mockSchoolEntity)
        val successResponse = Response.success(mockSchools)
        Mockito.`when`(repository.getSchools()).thenReturn(successResponse)

        viewModel.getSchoolList()
        testDispatcher.scheduler.advanceUntilIdle()

        assert(viewModel.schoolList == mockSchools)
    }

    @Test
    fun `test getSchoolList() error`() = runTest {
        Mockito.`when`(repository.getSchools()).thenThrow(RuntimeException())

        viewModel.getSchoolList()
        testDispatcher.scheduler.advanceUntilIdle()

        assert(viewModel.error)
    }
}