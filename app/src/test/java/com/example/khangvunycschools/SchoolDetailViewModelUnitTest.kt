package com.example.khangvunycschools

import com.example.khangvunycschools.data.repo.RepositoryImpl
import com.example.khangvunycschools.models.ScoreEntity
import com.example.khangvunycschools.util.MainDispatcherRule
import com.example.khangvunycschools.vm.SchoolDetailViewModel
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import retrofit2.Response

class SchoolDetailViewModelUnitTest {
    @get:Rule
    val mainDispatcherRule = MainDispatcherRule()

    @Mock
    lateinit var repository: RepositoryImpl
    private val testDispatcher = StandardTestDispatcher()
    private val mockScoreEntity: ScoreEntity = Mockito.mock()
    private val mockDbn: String = "Mock String"
    private lateinit var viewModel: SchoolDetailViewModel

    @Before
    fun setup() {
        MockitoAnnotations.openMocks(this)
        viewModel = SchoolDetailViewModel(repository)
    }

    @Test
    fun `test getScore success`() = runTest {
        val mockScores = listOf(mockScoreEntity, mockScoreEntity)
        val successResponse = Response.success(mockScores)
        Mockito.`when`(repository.getScore(mockDbn)).thenReturn(successResponse)

        viewModel.getScore(mockDbn)
        testDispatcher.scheduler.advanceUntilIdle()
        assert(viewModel.scores == mockScores.first())
    }

    @Test
    fun `test getScore error`() = runTest {
        Mockito.`when`(repository.getScore(mockDbn)).thenThrow(RuntimeException())

        viewModel.getScore(mockDbn)
        testDispatcher.scheduler.advanceUntilIdle()
        assert(viewModel.error)
    }
}