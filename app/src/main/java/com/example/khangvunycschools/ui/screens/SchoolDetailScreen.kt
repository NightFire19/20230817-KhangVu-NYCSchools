package com.example.khangvunycschools.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.khangvunycschools.vm.SchoolDetailViewModel

//Screen that holds the SAT scores.
@Composable
fun SchoolDetailScreen (
    dbn: String,
    navController: NavController,
    viewModel: SchoolDetailViewModel = hiltViewModel()
) {
    viewModel.getScore(dbn)

    Column (
        modifier = Modifier.fillMaxWidth()
            ) {
        if (viewModel.error) {
            Text("An error occurred. Check your connection.")
        } else {
            Row (
                Modifier.background(color = MaterialTheme.colors.primary).fillMaxWidth()
                    ){
                IconButton(
                    onClick = {
                        navController.popBackStack()
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "",
                        tint = MaterialTheme.colors.onPrimary
                    )
                }
                Text(
                    text = viewModel.scores.schoolName,
                    color = MaterialTheme.colors.onPrimary
                )
            }
            if (viewModel.noData) {
                Text("No data for this school.")
            } else {
                if (viewModel.scores.schoolName == "") {
                    Text("Loading...")
                } else {
                    Row (
                        modifier = Modifier.fillMaxWidth().padding(8.dp),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ){
                        Column {
                            Text("SAT Test Takers:")
                            Text("SAT Math Score:")
                            Text("SAT Critical Reading Score:")
                            Text("SAT Writing Score:")
                        }
                        Column {
                            Text(viewModel.scores.numberOfSatTestTakers)
                            Text(viewModel.scores.satMathAvgScore)
                            Text(viewModel.scores.satCriticalReadingAvgScore)
                            Text(viewModel.scores.satWritingAvgScore)
                        }

                    }
                }
            }
        }
    }


}

