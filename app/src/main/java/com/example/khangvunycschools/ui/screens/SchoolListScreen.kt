package com.example.khangvunycschools.ui.screens

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.khangvunycschools.models.SchoolEntity
import com.example.khangvunycschools.vm.SchoolListViewModel

//Screen that holds the list of NYC schools.
@Composable
fun SchoolListScreen(
    navController: NavController,
    viewModel: SchoolListViewModel = hiltViewModel()
) {
    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        if (viewModel.error) {
            Text("An error occurred. Check your connection.")
        } else {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(color = MaterialTheme.colors.primary),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                Text(
                    text = "NYC High Schools",
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onPrimary
                )
                Text(
                    text = "Select a school to see its SAT Scores:",
                    textAlign = TextAlign.Center,
                    color = MaterialTheme.colors.onPrimary
                )

            }
            if (viewModel.schoolList.isEmpty()) {
                Text("Loading...")
            } else {
                LazyColumn(
                    modifier = Modifier.fillMaxWidth()
                ) {
                    items(
                        items = viewModel.schoolList,
                        itemContent = { school ->
                            SchoolCard(schoolEntity = school, navController = navController)
                        }
                    )
                }
            }
        }
    }
}

@Composable
fun SchoolCard(schoolEntity: SchoolEntity, navController: NavController) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable {
                navController.navigate(
                    "schoolDetailScreen/{dbn}"
                        .replace(
                            oldValue = "{dbn}",
                            newValue = schoolEntity.dbn
                        )
                )
            },
        backgroundColor = MaterialTheme.colors.secondary
    ) {
        Text(
            text = schoolEntity.school_name,
            color = MaterialTheme.colors.onSecondary
        )
    }
}