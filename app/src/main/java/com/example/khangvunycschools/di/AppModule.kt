package com.example.khangvunycschools.di

import com.example.khangvunycschools.data.remote.ApiService
import com.example.khangvunycschools.util.Constants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


//Informs Dagger/Hilt how to provide instances of certain types.
@Module
@InstallIn(SingletonComponent::class) //SingletonComponent is a Hilt component that is an injector for Application
object AppModule {

    //Using @Provides annotation for Hilt to provide API instance.
    @Provides
    @Singleton
    fun provideApiService(): ApiService {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiService::class.java)
    }
}