package com.example.khangvunycschools.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.khangvunycschools.navigation.Screens.SchoolList.route
import com.example.khangvunycschools.ui.screens.SchoolDetailScreen
import com.example.khangvunycschools.ui.screens.SchoolListScreen

//NavGraph holding destinations.
@Composable
fun NavGraph (
    navController: NavHostController,
) {
    NavHost(
        navController = navController,
        startDestination = route,
        ) {
        composable(route = Screens.SchoolList.route) {
            SchoolListScreen(navController = navController)
        }
        composable("schoolDetailScreen/{dbn}") {navBackStackEntry ->
            val dbn = navBackStackEntry.arguments?.getString("dbn")

            if (dbn != null) {
                SchoolDetailScreen(dbn = dbn, navController = navController)
            }
        }
    }
}