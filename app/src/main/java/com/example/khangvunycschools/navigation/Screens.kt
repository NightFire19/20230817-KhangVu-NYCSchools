package com.example.khangvunycschools.navigation

//Sealed Screens class for navigation
sealed class Screens (val route: String) {
    object SchoolList: Screens(route = "school_list_screen")
}
