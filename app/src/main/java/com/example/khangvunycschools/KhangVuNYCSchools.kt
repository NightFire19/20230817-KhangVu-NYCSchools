package com.example.khangvunycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
//Triggers Hilt code generation for a base class that is the application-level dependency container.
class KhangVuNYCSchools : Application() {
}