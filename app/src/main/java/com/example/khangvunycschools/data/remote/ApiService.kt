package com.example.khangvunycschools.data.remote

import com.example.khangvunycschools.models.SchoolEntity
import com.example.khangvunycschools.models.ScoreEntity
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

//API that gets response from server
interface ApiService {

    //Get list of NYC Schools
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(): Response<List<SchoolEntity>>

    //Get SAT scores of school by dbn using Query
    @GET("resource/f9bf-2cp4.json")
    suspend fun getScore(@Query("dbn") dbn: String): Response<List<ScoreEntity>>
}