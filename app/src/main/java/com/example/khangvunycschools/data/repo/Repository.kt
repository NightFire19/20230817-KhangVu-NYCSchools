package com.example.khangvunycschools.data.repo

import com.example.khangvunycschools.models.SchoolEntity
import com.example.khangvunycschools.models.ScoreEntity
import retrofit2.Response

interface Repository {
    suspend fun getSchools(): Response<List<SchoolEntity>>
    suspend fun getScore(dbn: String): Response<List<ScoreEntity>>
}