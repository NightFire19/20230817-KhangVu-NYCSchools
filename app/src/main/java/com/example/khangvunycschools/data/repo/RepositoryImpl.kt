package com.example.khangvunycschools.data.repo

import android.util.Log
import com.example.khangvunycschools.data.remote.ApiService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

//Repository layer
class RepositoryImpl  @Inject constructor(
    private val apiService: ApiService
) : Repository {
    override suspend fun getSchools() = withContext(Dispatchers.IO) {
        return@withContext apiService.getSchools()
    }

    override suspend fun getScore(dbn: String) = withContext(Dispatchers.IO) {
        return@withContext apiService.getScore(dbn)
    }
}