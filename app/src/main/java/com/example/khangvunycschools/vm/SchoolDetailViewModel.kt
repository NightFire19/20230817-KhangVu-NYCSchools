package com.example.khangvunycschools.vm

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.khangvunycschools.data.repo.RepositoryImpl
import com.example.khangvunycschools.models.ScoreEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


//ViewModel for SchoolDetail Screen
@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val repository: RepositoryImpl,
) : ViewModel() {
    var scores: ScoreEntity by mutableStateOf(
        ScoreEntity()
    )
        private set

    var error: Boolean by mutableStateOf(false)
        private set

    var noData: Boolean by mutableStateOf(false)
        private set

    fun getScore(dbn: String) = viewModelScope.launch {
        //try catch block if apiService throws exception (no internet connection)
        try {
            repository.getScore(dbn).body()?.let { response ->
                if (response.isNotEmpty()) {
                    scores = response.first()
                } else {
                    noData = true
                }
            }
        } catch (exception: Exception) {
            error = true
        }

    }
}