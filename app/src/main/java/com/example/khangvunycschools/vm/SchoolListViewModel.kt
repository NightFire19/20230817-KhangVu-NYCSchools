package com.example.khangvunycschools.vm

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.khangvunycschools.data.repo.RepositoryImpl
import com.example.khangvunycschools.models.SchoolEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject
//ViewModel for School List Screen
@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val repository: RepositoryImpl,
) : ViewModel() {

    var schoolList: List<SchoolEntity> by mutableStateOf(listOf())
        private set

    var error: Boolean by mutableStateOf(false)
        private set
    init {
        getSchoolList()
    }

    //if application was more complex use cases may be used, however this is a simple app.
    fun getSchoolList() = viewModelScope.launch {
        try {
            repository.getSchools().body()?.let { responseList ->
                schoolList = responseList
            }
        } catch (exception: Exception) {
            error = true
        }
    }
}