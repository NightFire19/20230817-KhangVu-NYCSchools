package com.example.khangvunycschools.models

import com.google.gson.annotations.SerializedName

//Data class for each school
data class SchoolEntity(
    @SerializedName("dbn")
    val dbn: String,
    @SerializedName("school_name")
    val school_name: String,
)
